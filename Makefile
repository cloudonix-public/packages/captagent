VERSION=6.3.0

all: repo

repo: deb-repo rpm-repo

deb-repo: ubuntu rebuild-deb-repo
	mkdir -p deb-repo/bionic

ubuntu:
	docker build -f ubuntu.Dockerfile -t captagent:ubuntu --build-arg VERSION=$(VERSION) .
	docker run -a stdout -a stderr --rm -v $(shell pwd)/deb-repo/bionic:/app/debs captagent:ubuntu cp ../captagent_$(VERSION)_amd64.deb /app/debs/

rebuild-deb-repo:
	docker run -a stdout -a stderr --rm -v $(shell pwd)/deb-repo:/repo ubuntu:18.04 bash -exc '\
		apt-get update && apt-get install -qy dpkg-dev; \
		cd /repo; \
		dpkg-scanpackages bionic /dev/null > ./Packages; \
		gzip -9c < ./Packages > ./Packages.gz \
	'

rpm-repo: centos rebuild-rpm-repo
	mkdir -p rpm-repo

centos:
	docker build -f centos.Dockerfile -t captagent:centos --build-arg VERSION=$(VERSION) .
	docker run -a stdout -a stderr --rm -v $(shell pwd)/rpm-repo:/app/rpms captagent:centos bash -exc '\
		cp -r /root/rpmbuild/SRPMS /app/rpms/; \
		cp -r /root/rpmbuild/RPMS/* /app/rpms/; \
	'

rebuild-rpm-repo:
	docker run -a stdout -a stderr --rm -v $(shell pwd)/rpm-repo:/repo centos:7 bash -exc '\
		yum install -y createrepo; \
		createrepo /repo/SRPMS; \
		createrepo /repo/x86_64; \
	'

rebuild-rpm-repo-monolithic:
	docker run -a stdout -a stderr --rm -v $(shell pwd)/rpm-repo:/repo centos:7 bash -exc '\
		yum install -y createrepo; \
		createrepo /repo; \
	'

clean:
	rm -rf deb-repo rpm-repo

.PHONY: all repo ubuntu centos
