Name: captagent
Version: 6.3.0
Release: 3%{?dist}
URL: https://github.com/sipcapture/captagent
Source0: https://github.com/sipcapture/captagent/archive/6.3.0.tar.gz

License: GPLv3
Summary: 100% Open-Source Packet Capture Agent for HEP https://sipcapture.org
BuildRequires: file, make, flex, automake, libtool, bison
BuildRequires: libuv-devel, openssl-devel, libgcrypt-devel, json-c-devel, expat-devel, libpcap-devel, flex-devel

%package devel
Summary: Development libraries for Captagent

%description
Captagent is a powerful, flexible, completely modular HEP packet capture and mirroring framework for RTC,.
ready for (virtually) any kind of IP protocol and encapsulation method - past, present - and future.

%description devel
Captagent development library

%prep
%setup -q

%build
./build.sh
%configure \
	--enable-compression \
	--enable-ipv6
make %{?_smp_mflags}
perl -pi -e 's,/opt/sbin/captagent,/usr/sbin/captagent,' pkg/el/7/captagent.service
perl -pi -e 's,CFG_FILE.*,CFG_FILE=/etc/captagent/captagent.xml,' pkg/el/captagent.sysconfig

%install
make install DESTDIR=$RPM_BUILD_ROOT
install -D -m644 pkg/el/captagent.sysconfig $RPM_BUILD_ROOT/etc/sysconfig/captagent
install -D -m644 pkg/el/7/captagent.service $RPM_BUILD_ROOT/lib/systemd/system/captagent.service

ls $RPM_BUILD_ROOT/usr/lib64/captagent/modules/*.so | sed "s,$RPM_BUILD_ROOT,," > sofiles
ls $RPM_BUILD_ROOT/usr/lib64/captagent/modules/*a | sed "s,$RPM_BUILD_ROOT,," > arfiles

%files -f sofiles
%defattr(-,root,root,-)
%config /etc/captagent/*
%config /etc/sysconfig/captagent
%attr(0755,-,-) /usr/sbin/captagent
/lib/systemd/system/captagent.service
%doc COPYING AUTHORS ChangeLog README.md

%files devel -f arfiles

%changelog
* Fri Jul 20 2018 Oded Arbel <odeda@cloudonix.io> 6.3.0-3
- fixed installation of sysconfig file

* Fri Jul 20 2018 Oded Arbel <odeda@cloudonix.io> 6.3.0-2
- Added service file
- Fixed configuration

* Fri Jul 20 2018 Oded Arbel <odeda@cloudonix.io> 6.3.0-1
- Initial CentOS package for Cloudonix
