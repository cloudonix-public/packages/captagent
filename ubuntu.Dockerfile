FROM ubuntu:18.04

RUN apt-get update && apt-get install -qy \
	curl libtool autoconf automake autogen flex bison \
	libexpat-dev libpcap-dev libjson-c-dev libfl-dev libgcrypt-dev libssl-dev libuv-dev make libz-dev \
	dh-make
ARG VERSION
WORKDIR /app/src
RUN curl -sfL https://github.com/sipcapture/captagent/archive/6.3.0.tar.gz | tar -zx
WORKDIR /app/src/captagent-$VERSION
ADD debian /app/src/captagent-$VERSION/debian
RUN dpkg-buildpackage -us -uc

#### How I create debian native package
#ENV LOGNAME root
#ENV DEBFULLNAME Oded Arbel
#ENV DEBEMAIL=odeda@cloudonix.io
#RUN dh_make --native --single --yes

### How I build without a package
#RUN sed -i 's|^.*fl, yywrap.*$||' configure.ac
#RUN ./build.sh
#RUN ./configure --enable-tls --enable-ssl --enable-compression --enable-ipv6
#RUN make LEX="flex --noyywrap"

