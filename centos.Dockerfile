FROM centos:7

RUN yum -y install epel-release rpm-build spectool
ARG VERSION
RUN mkdir -p $HOME/rpmbuild/{BUILD,RPMS,SOURCES,SPECS,SRPMS}
ADD centos/captagent.spec root/rpmbuild/SPECS/
RUN yum-builddep -y root/rpmbuild/SPECS/captagent.spec
RUN spectool -g -R root/rpmbuild/SPECS/captagent.spec
RUN rpmbuild -ba root/rpmbuild/SPECS/captagent.spec
