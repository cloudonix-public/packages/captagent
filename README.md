# captagent builder

This repository uses a Docker environment to build packages for SipCapture's captagent.

Usage:

```
docker build -f ubuntu.Dockerfile -t captagent --build-arg VERSION=6.3.0 .
```


